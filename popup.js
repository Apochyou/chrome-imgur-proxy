
/* global */
var gPort = chrome.extension.connect({name: 'imgur-proxy'});
var gServerInfo;
var gPreferServer;
var gInitShowPreferServer = false;
var gDataRetrievalIntervalHandler;

var dom = {
	cb: document.getElementById('prefer-server-auto'), /* Checkbox: auto */
	st: document.getElementById('prefer-server'), /* Select: prefer server */
	bn: document.getElementById('remeasure-response-time') /* Button: remeasure */
};


function startUpdateData() {
	gDataRetrievalIntervalHandler = setInterval(function() {
		gPort.postMessage({request: 'getServerInfo'});
		gPort.postMessage({request: 'getPreferServer'});
	}, 1000);
}
function stopUpdateData() {
	clearInterval(gDataRetrievalIntervalHandler);
}

/* handle response from background */
gPort.onMessage.addListener(function(msg) {
	switch (msg.request) {
		case 'getServerInfo':
			gServerInfo = msg.response;
			showResponseTime();
		break;
		case 'getPreferServer':
			gPreferServer = msg.response;
			if (gPreferServer.length == 0)
				return;
			//console.debug('getPreferServer auto: ' + gPreferServer.auto.name + ', manual: ' + gPreferServer.manual.name);
			showPreferServer(gPreferServer.manual? 'manual' : 'auto', !gInitShowPreferServer);
			gInitShowPreferServer = true;
		break;
	}
});

/* build select options */
(function () {
	for (i = 0; i < ProxySites.length; i++) {
		var ps = ProxySites[i];
		if (ps.servers.length > 1) {
			var gr = document.createElement('optgroup');
			gr.label = ps.name;
			for (j = 0; j < ps.servers.length; j++) {
				gr.appendChild(new Option(ps.servers[j].description, ps.servers[j].name));
			}
			dom.st.add(gr);
		} else {
			/* NOTE: no description in single server side */
			dom.st.add(new Option(ps.servers[0].name, ps.servers[0].name))
		}
	}
})();

/* handle auto checkbox */
dom.cb.onclick = function () {
	stopUpdateData();
	if (this.checked) {
		showPreferServer('auto', true);
	} else {
		showPreferServer('manual', true);
	}
	startUpdateData();
}

/* handle select of prefer server */
dom.st.onchange = function () {
	stopUpdateData();
	gPort.postMessage({request: 'setPreferServer', value: this.value});
	startUpdateData();
}

/* handle remeasure button */
dom.bn.onclick = function () {
	/* set status to measuring if background is ok to do remeasure */
	this.disabled = true;
	this.textContent = 'Measuring..';
	var as = document.getElementsByClassName('status');
	for (i = 0; i < as.length; i++) {
		as[i].innerHTML = '<span>' + chrome.i18n.getMessage('popMeasuring') + '</span>';
	}
	gPort.postMessage({request: 'doRemeasure'});
}

/* to show prefer server, update select and checkbox  */
function showPreferServer(t, u) {
	if (t == 'auto') {
		if (gPreferServer.auto && dom.cb.checked)
			dom.st.value = gPreferServer.auto.name;
		if (u) {
			dom.cb.checked = true;
			dom.st.disabled = true;
			gPort.postMessage({request: 'setPreferServer', value: undefined});
		}
	} else if (t == 'manual') {
		if (gPreferServer.manual)
			dom.st.value = gPreferServer.manual.name;
		if (u) {
			dom.cb.checked = false;
			dom.st.disabled = false;
			gPort.postMessage({request: 'setPreferServer', value: dom.st.value});
		}
	}
}

/* build server list */
(function () {
	var sl = document.getElementById('server-list');
	for (i = 0; i < ProxySites.length; i++) {
		var ps = ProxySites[i];
		/* for sites */
		sw = document.createElement('div');
		sw.className = 'side';
		sn = document.createElement('span');
		sn.textContent = ps.name + ': ';
		sn.className = 'desc';
		sw.appendChild(sn);
		ss = document.createElement('span');
		ss.textContent = chrome.i18n.getMessage('popUnavailable');
		ss.id = ps.name + '-status';
		sw.appendChild(ss);
		sl.appendChild(sw);
		if (ps.servers.length == 1) {
			/* only one server */
			ss.id = ps.servers[0].name + '-status';
			ss.className = 'status';
		} else {
			/* for more than one server */
			ss.textContent = '';
			as = document.createElement('div');
			as.className = 'servers';
			for (j = 0; j < ps.servers.length; j++) {
				sw = document.createElement('div');
				sw.className = 'sub';
				sn = document.createElement('span');
				sn.textContent = ps.servers[j].description + ': ';
				sn.className = 'desc';
				sw.appendChild(sn);
				ss = document.createElement('span');
				ss.className = 'status';
				ss.innerHTML = '<span style="color:red">' + chrome.i18n.getMessage('popUnavailable') + '</span>';
				ss.id = ps.servers[j].name + '-status';
				sw.appendChild(ss);
				as.appendChild(sw);
			}
			sl.appendChild(as);
		}
	}
})();

/* load response time of servers */
function showResponseTime() {
	var measureCount = 0;
	for (i = 0; i < gServerInfo.length; i++) {
		s = gServerInfo[i];
		ss = document.getElementById(s.name + '-status');
		if (s.isAccessible && !s.isMeasuring)
			ss.innerHTML = '<span style="color:' + (s.responseTime < 1000? 'green' : (s.responseTime < 2000? 'orange':'red')) +'">' + s.responseTime + 'ms</span>';
		else if (s.isMeasuring) {
			ss.innerHTML = '<span>' + chrome.i18n.getMessage('popMeasuring') + '</span>';
			measureCount++;
		} else
			ss.innerHTML = '<span style="color:red">' + chrome.i18n.getMessage('popUnavailable') + '</span>';
	}

	/* no server is measuring */
	if (!measureCount) {
		dom.bn.disabled = false;
		dom.bn.textContent = chrome.i18n.getMessage('popRemeasure');
	} else {
		dom.bn.textContent = chrome.i18n.getMessage('popMeasuring');
	}
}

document.getElementById('prefer-server-auto-label').textContent = chrome.i18n.getMessage('popAutoSelect');

gPort.postMessage({request: 'getServerInfo'});
gPort.postMessage({request: 'getPreferServer'});
startUpdateData();
