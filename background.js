function secs(s) {return s * 1000;}
function mins(m) {return m * secs(60);}

/* const */
const IMGUR_TEST = {
	'url': 'https://s.imgur.com/images/favicon-16x16.png',
	'content-type': 'image/png'
};
const MEASURE_INTERVAL = mins(10);
const CACHE_TIMEOUT = mins(20);

/* global */
var gMeasureIntervalHandler;

axios.defaults.timeout = secs(20);

Date.prototype.toString = function () {
	var time = ('00' + this.getHours()).slice(-2) + ':' + ('00' + this.getMinutes()).slice(-2) + ':' + ('00' + this.getSeconds()).slice(-2);
	time += '.' + ('000' + this.getMilliseconds()).slice(-3);
	return this.getFullYear() + '-' + this.getMonth() + '-' + this.getDate() + ' ' + time;
};

/* handle request from popup */
chrome.runtime.onConnect.addListener(function(port) {
	if (port.name != 'imgur-proxy')
		return;
	port.onMessage.addListener(function(msg) {
		switch (msg.request) {
			case 'getServerInfo':
				msg.response = Server._list;
				port.postMessage(msg);
			break;
			case 'getPreferServer':
				msg.response = {auto: Server.getPrefer('auto'), manual: Server.getPrefer('manual')};
				port.postMessage(msg);
			break;
			case 'setPreferServer':
				console.debug('setPreferServer: ' + msg.value);
				if (!msg.value) {
					chrome.storage.sync.remove('preferServer');
					Server.setPrefer('manual', undefined);
				} else {
					chrome.storage.sync.set({preferServer: msg.value});
					Server.setPrefer('manual', Server.getByName(msg.value));
				}
			break;
			case 'doRemeasure':
				selectPreferSever();
			break;
		}
	});
});

/* ==== Proxy Sites ==== */
function Server(sn, uf, s) {
	this.sideName = sn,
	this.urlFilter = uf;
	this.name = s.name;
	this.url = s.url;
	this.postData = s.postData;
	this.genPostData = function(url) {
		var params = new URLSearchParams();
		for (p in this.postData) {
			if (this.postData[p] == '__URL__')
				params.append(p, url);
			else
				params.append(p, this.postData[p]);
		}
		return params;
	}
	this.genGETUrl = function (url) {
		return this.url.replace('__URL__', url);
	};
	this.reset = function () {
		this.isAccessible = false;
		this.isMeasuring = false;
		this.responseTime = secs(60); /* time to access target via proxy */
	};
	this.reset();
}

Server._list = [];
Server.forEach = function(cb) {
	Server._list.forEach(cb);
};
Server._prefer = {'auto': undefined, 'manual': undefined};
Server.setPrefer = function (t, s) {
	var _p = Server._prefer;
	if (t == 'auto') {
		if (!_p.auto) {
			console.log('[' + s.name + '][' + s.responseTime + '] is prefer server due to auto is empty now');
			_p.auto = s;
		} else if (s.responseTime < _p.auto.responseTime) {
			console.log('[' + s.name + '][' + s.responseTime + '] is better than [' + _p.auto.name + '][' + _p.auto.responseTime + ']');
			if (!_p.manual && _p.auto.name != s.name)
				ProxyImgur.flush();
			_p.auto = s;
		}
		return s;
	} else if (t == 'manual') {
		if ((!s && _p.auto && _p.manual && _p.auto.name != _p.manual.name)
				|| (!_p.manual && _p.auto && s && _p.auto.name != s.name)
				|| (_p.manual && s && _p.manual.name != s.name))
			ProxyImgur.flush();
		_p.manual = s;
		return s;
	}
};
Server.getPrefer = function (t) {
	var _p = Server._prefer;
	if (t == 'auto')
		return _p.auto;
	else if (t == 'manual')
		return _p.manual;
	else
		return _p.manual? _p.manual : _p.auto;
};
Server.getURLFilters = function() {
	var filters = [];
	for (s = 0; s < Server._list.length; s++) {
		if (!filters[0]) {
			filters.push(Server._list[s].urlFilter);
			continue;
		}
		if (filters[filters.length - 1] == Server._list[s].urlFilter)
			continue;
		filters.push(Server._list[s].urlFilter);
	}
	return filters;
};
Server.getByName = function(n) {
	for (i = 0; i < Server._list.length; i++) {
		if (n == Server._list[i].name)
			return Server._list[i];
	}
	return undefined;
};
Server.isMeasuring = function() {
	for (i = 0; i < Server._list.length; i++) {
		if (Server._list[i].isMeasuring)
			return true;
	}
	return false;
};

/* load server info from proxy-sites.js */
(function(){
	for (var i = 0; i < ProxySites.length; i++) {
		for (var j = 0; j < ProxySites[i].servers.length; j++) {
			Server._list.push(new Server(ProxySites[i].name, ProxySites[i].urlFilter, ProxySites[i].servers[j]));
		}
	}
})();

/* load configurations */
chrome.storage.sync.get(
	'preferServer'
	, function(config){
		console.debug('chrome.storage.sync.get: preferServer = ' + config.preferServer);
		if (config.preferServer && Server.getByName(config.preferServer)) {
			Server.setPrefer('manual', Server.getByName(config.preferServer));
			console.debug('load: config.preferServer = ' + Server.getPrefer('manual').name);
		} else if (config.preferServer) {
			/* not a corret server in server list */
			chrome.storage.sync.remove('preferServer');
		}
	});

/* 
 * ==== Proxy Imgur ==== 
 */
function ProxyImgur(url, ourl) {
	this.url = url;
	this.originURL = ourl;
	this.id = parseImgurId(url);
	this.proxyURL = undefined;
	this.reqestList = {};
	this.queueReqest = function (tabId, url) {
		//console.debug('queue request => ' + tabId + ' : ' + url);
		if (this.reqestList[tabId]) {
			if (this.reqestList[tabId].indexOf(url) == -1)
				this.reqestList[tabId].push(url);
		} else {
			this.reqestList[tabId] = [];
			this.reqestList[tabId][0] = url;
		}
	};
	this.doProxy = function (url) {
		this.proxyURL = url;
		/* send message to tabs to update imgur url */
		//console.debug('do proxy ' + this.url + ' => ' + this.proxyURL);
		for (tabId in this.reqestList) {
			this.reqestList[tabId].forEach((function (pi) {
				return function (url) {
					/* for tabs access imgur direcly */
					chrome.tabs.get(parseInt(tabId), function (tab) {
						if (tab.url.indexOf(chrome.runtime.id) != -1)
							chrome.tabs.update(tab.id, {'url': pi.proxyURL});
					});
					/* for tabs contain several imgur images */
					chrome.tabs.sendMessage(parseInt(tabId), {'url': url, 'proxyURL': pi.proxyURL});
			}})(this));
		}
	};

	/* delete entry when time out */
	this.cacheHandler = 0;
	this.cleanCacheTimeout = function() {
		clearTimeout(this.cacheHandler);
	};
	this.renewCache = function (t) {
		this.cleanCacheTimeout();
		this.cacheHandler = setTimeout(
			(function (pi) {return function () {ProxyImgur.del(pi.url);}})(this)
		, t? t : CACHE_TIMEOUT);
	};
	this.renewCache();
}

ProxyImgur._list = {};
ProxyImgur.add = function(url) {
	var id = parseImgurId(url);
	var mURL = parseImgurURL(url);
	
	/* couldn't parse the url, nothing to do */
	if (!id || !mURL)
		return undefined;

	if (!ProxyImgur._list[id])
		ProxyImgur._list[id] = new ProxyImgur(mURL, url);
	return ProxyImgur._list[id];
};
ProxyImgur.get = function(url) {
	var id = parseImgurId(url);
	return ProxyImgur._list[id];
};
ProxyImgur.del = function(url) {
	var id = parseImgurId(url);
	console.debug('delete cache of ' + url + ' [' + id + ']');
	ProxyImgur._list[id].cleanCacheTimeout();
	delete ProxyImgur._list[id];
};
ProxyImgur.flush = function() {
	console.debug('flush proxy cache');
	for (id in ProxyImgur._list) {
		ProxyImgur.del(id);
	}
};

function _getResponseFromServer(server, sendDate) {
	return function (response) {
		//console.debug('[' + server.name + '] ' + response.config.url);
		/* analyze the response */
		if (response.headers['content-type'] != IMGUR_TEST['content-type']) {
			/* not a correct response */
			console.log('[' + server.name + '] response with incorrect content-type [' + response.headers['content-type'] + ']');
			//console.log(response);
			server.isMeasuring = false;
			return;
		}
		server.responseTime = (new Date()) - sendDate;
		console.log('[' + server.name + '] response time = ' + server.responseTime);
		Server.setPrefer('auto', server);
		server.isAccessible = true;
		server.isMeasuring = false;
	}
}

function selectPreferSever() {
	/* check server accessible and get response time */
	if (Server.isMeasuring()) {
		console.log('measuring task is still ongoing');
		return;
	}
	console.log('measure response time of servers <' + new Date() + '>');
	Server.forEach(function (server){
		server.reset();
		server.isMeasuring = true;

		var sendDate = new Date();
		console.debug('[' + server.name + '] measuring from <' + sendDate + '>');
		if (!server.postData) {
			/* use GET to fetch proxy url */
			axios.get(server.genGETUrl(IMGUR_TEST.url), {timeout: secs(8)})
			.then(_getResponseFromServer(server, sendDate))
			.catch(function (error) {
				server.isMeasuring = false;
			});
		} else {
			/* use POST to fetch proxy url */
			axios.post(server.url, server.genPostData(IMGUR_TEST.url), {timeout: secs(8)})
			.then(_getResponseFromServer(server, sendDate))
			.catch(function (error) {
				server.isMeasuring = false;
			});
		}
	});
}
selectPreferSever();
gMeasureIntervalHandler = setInterval(selectPreferSever, MEASURE_INTERVAL);

function parseImgurId(url) {
	var result = /.*:\/\/.*imgur\.com\/([^.]*)/.exec(url);
	return result? result[1] : url;
}

function parseImgurURL(url) {
	var newURL = url.replace(/.*:\/\/([im]\.)*imgur\.com\/(a\/)*([^\.]*)(\..*)*/, 
		function (m, i, a, c, s) {
			//console.log(i, a, c, s);
			/* there is a / in content, wrong format */
			if (/\//.test(c))
				return '';
			/* do nothing when browser GET .ico */
			if (s == '.ico')
				return '';
			/* auto append suffix */
			if (!s)
				s = '.jpg'
			
			return 'https://i.imgur.com/' + c + s;
		});
	return newURL.length? newURL : undefined;
}

function getProxyURL(pi) {
	var ps = Server.getPrefer();
	console.debug('URL request: ' + pi.url + ' [' + pi.id + '] (origin: ' + pi.originURL + ') to [' + ps.name + '] <' + new Date() + '>');
	if (!ps.postData) {
		/* use GET */
		axios.get(ps.genGETUrl(pi.url))
		.then(function (response) {
			console.debug(pi.url + ' [' + pi.id + ']' + ' => ' + response.request.responseURL);
			pi.doProxy(response.request.responseURL);
		})
		.catch(function (error) {
			console.warn('fail to get proxy url', pi, error);
			/* fail to get proxy url, clean the proxy item in 3 seconds */
			pi.doProxy(chrome.extension.getURL('images/failed.png'));
			pi.renewCache(secs(3));
		});
	} else {
		/* use POST */
		axios.post(ps.url, ps.genPostData(pi.url))
		.then(function (response) {
			console.debug(pi.url + ' [' + pi.id + ']' + ' => ' + response.request.responseURL);
			pi.doProxy(response.request.responseURL);
		})
		.catch(function (error) {
			console.warn('fail to get proxy url', pi, error);
			/* fail to get proxy url, clean the proxy item in 3 seconds */
			pi.doProxy(chrome.extension.getURL('images/failed.png'));
			pi.renewCache(secs(3));
		});
	}
}

/* don't include "Referer" in request header to prevent imgur.com blcok it */
chrome.webRequest.onBeforeSendHeaders.addListener(
	function(info) {
		for (var i = 0; i < info.requestHeaders.length; ++i) {
			if (info.requestHeaders[i].name === 'Referer') {
				info.requestHeaders.splice(i, 1);
				break;
			}
		}
		return {requestHeaders: info.requestHeaders};
	},
	{urls: ['*://*.imgur.com/*', '*://imgur.com/*']},
	['blocking', 'requestHeaders', 'extraHeaders']
);

chrome.webRequest.onBeforeRequest.addListener(
	function(info) {
		//console.debug('onBeforeRequest', info);
		if (info.url == IMGUR_TEST.url)
			return {};
		
		/* always use our own embed.js */
		if (/embed\.js/.test(info.url)) {
			//console.debug('someone need embed.js');
			info.url = chrome.extension.getURL('embed-imgur.js');
			return {redirectUrl: info.url};
		}
		
		/* no prefer server available or prefer imgur.com, skip it */
		if (!Server.getPrefer() || Server.getPrefer().name == 'imgur.com') {
			console.log(info);
			return {};
		}
		
		//console.debug('request url: ' + info.url);
		/* check if the imgur id is already in list */
		var pi = ProxyImgur.get(info.url);
		if (pi) {
			if (pi.proxyURL) {
				/* already cached */
				console.debug('[Cached] ' + pi.url + ' ['+ pi.id +'] => ' + pi.proxyURL);
				pi.renewCache();
				return {redirectUrl: pi.proxyURL};
			} else {
				/* still ongoing to fetch proxy URL */
				console.debug('waiting proxy URL for ' + pi.url + ' ['+ pi.id +']');
				pi.queueReqest(info.tabId, info.url);
				return {redirectUrl: chrome.extension.getURL('images/loading.png')};
			}
		} else {
			/* not yet in list, create and queue it */
			pi = ProxyImgur.add(info.url);
			if (!pi) {
				/* wrong format, could not do anything, skip it */
				console.warn('[' + info.url + '] is not a valid URL');
				return {};
			}
			pi.queueReqest(info.tabId, info.url);
		}

		getProxyURL(pi);
		
		return {redirectUrl: chrome.extension.getURL('images/loading.png')};
	},
	{urls: ['*://*.imgur.com/*', '*://imgur.com/*']},
	['blocking']
);

/* debug loggers */
//chrome.webRequest.onBeforeRequest.addListener(function(i){console.debug('onBeforeRequest:',i);},{urls: Server.getURLFilters()},['blocking','requestBody']);
//chrome.webRequest.onBeforeRedirect.addListener(function(i){console.debug('onBeforeRedirect:',i);},{urls: Server.getURLFilters()},['responseHeaders']);
//chrome.webRequest.onHeadersReceived.addListener(function(i){console.debug('onHeadersReceived:',i);},{urls: Server.getURLFilters()},['blocking','responseHeaders']);
//chrome.webRequest.onBeforeRedirect.addListener(function(i){console.debug('onBeforeRedirect:',i);},{urls: ['*://*.imgur.com/*', '*://imgur.com/*']},['responseHeaders']);
//chrome.webRequest.onHeadersReceived.addListener(function(i){console.debug('onHeadersReceived:',i);},{urls: ['*://*.imgur.com/*', '*://imgur.com/*']},['blocking','responseHeaders']);