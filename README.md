# chrome-imgur-proxy

You can't block Imgur if I want.

Pick BEST web proxy automatically and access imgur via proxy,  
instead of redirect request to filmot.org.  
More secure and more stable.

source code: https://gitlab.com/Apochyou/chrome-imgur-proxy

List of web proxy we use:  
~~dualnitro (https://dualnitro.com/)~~  
free-proxy (https://www.free-proxy.com/)  
hide.me (https://hide.me/en/proxy)  
~~hidemyass (https://www.hidemyass.com/proxy)~~  
hidester (https://hidester.com/)  
proxsei (https://www.proxsei.com/)  
proxypx (https://proxypx.com/)  
proxysite (https://www.proxysite.com/)  
stopcensoring.me (https://stopcensoring.me/)  
toolur (https://proxy.toolur.com/)  
unblockvideos (https://unblockvideos.com/)  
unblockyoutube / proxy-youtube / my-proxy (https://unblockyoutube.video/)  
vpnbook (https://www.vpnbook.com/webproxy)  
~~whoer (https://whoer.net/webproxy)~~  
zend2 (https://zend2.com)  

Please let me known if you have any prefered web proxy server.
