var ProxySites = [
/* ==== imgur.com ==== */
{
	name: 'imgur.com (no proxy)', urlFilter: '*://imgur.com/*',
	servers: [{
		name: 'imgur.com', 
		url: '__URL__'
	}]
},
/* ==== dualnitro (https://dualnitro.com/) ==== */
/*{
	name: 'dualnitro', urlFilter: '*://*.dualnitro.com/*',
	servers: [{
		name: 'dualnitro-ny', description: 'New York',
		url: 'https://ny-usa-emb.dualnitro.com/index.php',
		postData: {url: '__URL__'}
	}, {
		name: 'dualnitro-piscataway', description: 'Piscataway',
		url: 'https://piscataway-usa-emb.dualnitro.com/index.php',
		postData: {url: '__URL__'}
	}]
},*/
/* ==== free-proxy (https://www.free-proxy.com/) ==== */
{
	name: 'free-proxy', urlFilter: '*://*.free-proxy.com/*',
	servers: [{
		name: 'free-proxy-us1', description: 'US1',
		url: 'https://us1.free-proxy.com/includes/process.php?action=update',
		postData: {u: '__URL__', ProxyServer: 'on', allowCookies: 'off'}
	}, {
		name: 'free-proxy-us2', description: 'US2',
		url: 'https://us2.free-proxy.com/includes/process.php?action=update',
		postData: {u: '__URL__', ProxyServer: 'on', allowCookies: 'off'}
	}, {
		name: 'free-proxy-us3', description: 'US3',
		url: 'https://us3.free-proxy.com/includes/process.php?action=update',
		postData: {u: '__URL__', ProxyServer: 'on', allowCookies: 'off'}
	}, {
		name: 'free-proxy-us4', description: 'US4',
		url: 'https://us4.free-proxy.com/includes/process.php?action=update',
		postData: {u: '__URL__', ProxyServer: 'on', allowCookies: 'off'}
	}, {
		name: 'free-proxy-us5', description: 'US5',
		url: 'https://us5.free-proxy.com/includes/process.php?action=update',
		postData: {u: '__URL__', ProxyServer: 'on', allowCookies: 'off'}
	}, {
		name: 'free-proxy-us6', description: 'US6',
		url: 'https://us6.free-proxy.com/includes/process.php?action=update',
		postData: {u: '__URL__', ProxyServer: 'on', allowCookies: 'off'}
	}, {
		name: 'free-proxy-eu2', description: 'EU2',
		url: 'https://eu2.free-proxy.com/includes/process.php?action=update',
		postData: {u: '__URL__', ProxyServer: 'on', allowCookies: 'off'}
	}]
},
/* ==== hide.me (https://hide.me/en/proxy) ==== */
{
	name: 'hide.me', urlFilter: '*://*.hideproxy.me/*',
	servers: [{
		name: 'hide.me-nl', description: 'Netherlands',
		url: 'https://nl.hideproxy.me/includes/process.php?action=update',
		postData: {u: '__URL__', proxy_formdata_server: 'nl', allowCookies: 0, encodeURL: 0, encodePage: 0, stripObjects: 0, stripJS: 0, go: ''}
	}, {
		name: 'hide.me-de', description: 'Germany',
		url: 'https://de.hideproxy.me/includes/process.php?action=update', 
		postData: {u: '__URL__', proxy_formdata_server: 'de', allowCookies: 0, encodeURL: 0, encodePage: 0, stripObjects: 0, stripJS: 0, go: ''}
	}, {
		name: 'hide.me-us', description: 'USA',
		url: 'https://us.hideproxy.me/includes/process.php?action=update', 
		postData: {u: '__URL__', proxy_formdata_server: 'us', allowCookies: 0, encodeURL: 0, encodePage: 0, stripObjects: 0, stripJS: 0, go: ''}
	}]
},
/* ==== hidemyass (https://www.hidemyass.com/proxy) ==== */
/*{
	name: 'hidemyass', urlFilter: '*://www.hidemyass-freeproxy.com/*',
	servers: [{
		name: 'hidemyass-us', description: 'USA',
		url: 'https://www.hidemyass-freeproxy.com/process/en-us', 
		postData: {'form[url]': '__URL__', 'form[isCookieDisabled]': 1, 'form[dataCenter]': 'us', 'terms-agreed': 1}
	}, {
		name: 'hidemyass-us2', description: 'USA2',
		url: 'https://www.hidemyass-freeproxy.com/process/en-us', 
		postData: {'form[url]': '__URL__', 'form[isCookieDisabled]': 1, 'form[dataCenter]': 'us2', 'terms-agreed': 1}
	}, {
		name: 'hidemyass-de', description: 'Germany',
		url: 'https://www.hidemyass-freeproxy.com/process/en-us', 
		postData: {'form[url]': '__URL__', 'form[isCookieDisabled]': 1, 'form[dataCenter]': 'de', 'terms-agreed': 1}
	}, {
		name: 'hidemyass-nl', description: 'Netherlands',
		url: 'https://www.hidemyass-freeproxy.com/process/en-us', 
		postData: {'form[url]': '__URL__', 'form[isCookieDisabled]': 1, 'form[dataCenter]': 'nl', 'terms-agreed': 1}
	}, {
		name: 'hidemyass-uk', description: 'UK',
		url: 'https://www.hidemyass-freeproxy.com/process/en-us', 
		postData: {'form[url]': '__URL__', 'form[isCookieDisabled]': 1, 'form[dataCenter]': 'uk', 'terms-agreed': 1}
	}, {
		name: 'hidemyass-cz', description: 'Czech',
		url: 'https://www.hidemyass-freeproxy.com/process/en-us', 
		postData: {'form[url]': '__URL__', 'form[isCookieDisabled]': 1, 'form[dataCenter]': 'cz', 'terms-agreed': 1}
	}]
},*/
/* ==== hidester (https://hidester.com/) ==== */
{
	name: 'hidester', urlFilter: '*://*.hidester.com/*',
	servers: [{
		name: 'hidester-us', description: 'US',
		url: 'https://us.hidester.com/do.php?action=go',
		postData: {u: '__URL__', langsel: 'en', encodeURL: 'on', allowCookies: 'off', stripJS: 'on'}
	}, {
		name: 'hidester-eu', description: 'EU',
		url: 'https://eu.hidester.com/do.php?action=go',
		postData: {u: '__URL__', langsel: 'en', encodeURL: 'on', allowCookies: 'off', stripJS: 'on'}
	}]
},
/* ==== proxsei (https://www.proxsei.com/) ==== */
{
	name: 'proxsei', urlFilter: '*://*.proxsei.com/*',
	servers: [{
		name: 'proxsei',
		url: 'https://www.proxsei.com/includes/process.php?action=update',
		postData: {u: '__URL__'}
	}]
},
/* ==== proxypx (https://proxypx.com/) ==== */
{
	name: 'proxypx', urlFilter: '*://*.proxypx.com/*',
	servers: [{
		name: 'proxypx',
		url: 'https://us.proxypx.com/browse.php',
		postData: {url: '__URL__'}
	}]
},
/* ==== proxysite (https://www.proxysite.com/) ==== */
{
	name: 'proxysite', urlFilter: '*://*.proxysite.com/*',
	servers: [{
		name: 'proxysite-eu1', description: 'EU1',
		url: 'https://eu1.proxysite.com/includes/process.php?action=update', 		
		postData: {d: '__URL__', 'server-option': 'eu1', allowCookies: 'off'}
	}, {
		name: 'proxysite-eu3', description: 'EU3',
		url: 'https://eu3.proxysite.com/includes/process.php?action=update', 		
		postData: {d: '__URL__', 'server-option': 'eu3', allowCookies: 'off'}
	}, {
		name: 'proxysite-eu5', description: 'EU5',
		url: 'https://eu5.proxysite.com/includes/process.php?action=update', 		
		postData: {d: '__URL__', 'server-option': 'eu5', allowCookies: 'off'}
	}, {
		name: 'proxysite-us1', description: 'US1',
		url: 'https://us1.proxysite.com/includes/process.php?action=update', 		
		postData: {d: '__URL__', 'server-option': 'us1', allowCookies: 'off'}
	}, {
		name: 'proxysite-us3', description: 'US3',
		url: 'https://us3.proxysite.com/includes/process.php?action=update', 		
		postData: {d: '__URL__', 'server-option': 'us3', allowCookies: 'off'}
	}, {
		name: 'proxysite-us5', description: 'US5',
		url: 'https://us5.proxysite.com/includes/process.php?action=update', 		
		postData: {d: '__URL__', 'server-option': 'us5', allowCookies: 'off'}
	}]
},
/* ==== stopcensoring.me (https://stopcensoring.me/) ==== */
{
	name: 'stopcensoring.me', urlFilter: '*://stopcensoring.me/*',
	servers: [{
		name: 'stopcensoring.me',
		url: 'https://stopcensoring.me/index.php',
		postData: {url: '__URL__'}
	}]
},
/* ==== toolur (https://proxy.toolur.com/) ==== */
{
	name: 'toolur', urlFilter: '*://*.toolur.com/*',
	servers: [{
		name: 'toolur-us1', description: 'US1',
		url: 'https://proxy-us1.toolur.com/includes/process.php?action=update', 
		postData: {u: '__URL__', ProxyServer: 'on', allowCookies: 'off'}
	}, {
		name: 'toolur-us2', description: 'US2',
		url: 'https://proxy-us2.toolur.com/includes/process.php?action=update', 
		postData: {u: '__URL__', ProxyServer: 'on', allowCookies: 'off'}
	}, {
		name: 'toolur-us3', description: 'US3',
		url: 'https://proxy-us3.toolur.com/includes/process.php?action=update', 
		postData: {u: '__URL__', ProxyServer: 'on', allowCookies: 'off'}
	}, {
		name: 'toolur-us4', description: 'US4',
		url: 'https://proxy-us4.toolur.com/includes/process.php?action=update', 
		postData: {u: '__URL__', ProxyServer: 'on', allowCookies: 'off'}
	}, {
		name: 'toolur-us5', description: 'US5',
		url: 'https://proxy-us5.toolur.com/includes/process.php?action=update', 
		postData: {u: '__URL__', ProxyServer: 'on', allowCookies: 'off'}
	}, {
		name: 'toolur-us7', description: 'US7',
		url: 'https://proxy-us7.toolur.com/includes/process.php?action=update', 
		postData: {u: '__URL__', ProxyServer: 'on', allowCookies: 'off'}
	}, {
		name: 'toolur-fr1', description: 'EU1',
		url: 'https://proxy-fr1.toolur.com/includes/process.php?action=update', 
		postData: {u: '__URL__', ProxyServer: 'on', allowCookies: 'off'}
	}, {
		name: 'toolur-fr2', description: 'EU2',
		url: 'https://proxy-fr2.toolur.com/includes/process.php?action=update', 
		postData: {u: '__URL__', ProxyServer: 'on', allowCookies: 'off'}
	}]
},
/* ==== unblockvideos (https://unblockvideos.com/) ==== */
{
	name: 'unblockvideos', urlFilter: '*://*.unblockvideos.com/*',
	servers: [{
		name: 'unblockvideos-us-arizona', description: 'US-Arizona',
		url: 'https://us-arizona.unblockvideos.com/index.php',
		postData: {url: '__URL__', location: 'us-arizona.unblockvideos.com'}
	}, {
		name: 'unblockvideos-us-arizona3', description: 'US-Arizona3',
		url: 'https://arizona3.unblockvideos.com/index.php',
		postData: {url: '__URL__', location: 'arizona3.unblockvideos.com'}
	}, {
		name: 'unblockvideos-us-west', description: 'US-West',
		url: 'https://us-west.unblockvideos.com/index.php',
		postData: {url: '__URL__', location: 'us-west.unblockvideos.com'}
	}, {
		name: 'unblockvideos-virginia', description: 'Virginia',
		url: 'https://proxypk.com/index.php',
		postData: {url: '__URL__', location: 'proxypk.com'}
	}, {
		name: 'unblockvideos-california', description: 'California',
		url: 'https://two.proxypk.com/index.php',
		postData: {url: '__URL__', location: 'two.proxypk.com'}
	}]
},
/* ==== unblockyoutube / proxy-youtube / my-proxy (https://unblockyoutube.video/) ==== */
{
	name: 'unblockyoutube', urlFilter: '*://*.unblockyoutube.video/*',
	servers: [{
		name: 'unblockyoutube-us5', description: 'US 5',
		url: 'https://us5.unblockyoutube.video/index.php',
		postData: {url: '__URL__', server: 'us5'}
	}, {
		name: 'unblockyoutube-us7', description: 'US 7',
		url: 'https://us7.unblockyoutube.video/index.php',
		postData: {url: '__URL__', server: 'us7'}
	}, {
		name: 'unblockyoutube-us9', description: 'US 9',
		url: 'https://us9.unblockyoutube.video/index.php',
		postData: {url: '__URL__', server: 'us9'}
	}, {
		name: 'unblockyoutube-us11', description: 'US 11',
		url: 'https://us11.unblockyoutube.video/index.php',
		postData: {url: '__URL__', server: 'us11'}
	}, {
		name: 'unblockyoutube-uk1', description: 'UK 1',
		url: 'https://uk1.unblockyoutube.video/index.php',
		postData: {url: '__URL__', server: 'uk1'}
	}]
},
/* ==== vpnbook (https://www.vpnbook.com/webproxy) ==== */
{
	name: 'vpnbook', urlFilter: '*://*.vpnbook.com/*',
	servers: [{
		name: 'vpnbook-us', description: 'US',
		url: 'https://usproxy.vpnbook.com/includes/process.php?action=update',
		postData: {u: '__URL__', webproxylocation: 'us'}
	}, {
		name: 'vpnbook-uk', description: 'UK',
		url: 'https://usproxy.vpnbook.com/includes/process.php?action=update',
		postData: {u: '__URL__', webproxylocation: 'uk'}
	}, {
		name: 'vpnbook-fr', description: 'FR',
		url: 'https://usproxy.vpnbook.com/includes/process.php?action=update',
		postData: {u: '__URL__', webproxylocation: 'fr'}
	}, {
		name: 'vpnbook-ca', description: 'Canada',
		url: 'https://usproxy.vpnbook.com/includes/process.php?action=update',
		postData: {u: '__URL__', webproxylocation: 'ca'}
	}]
},
/* ==== zend2 (https://zend2.com) ==== */
{
	name: 'zend2', urlFilter: '*://*.zend2.com/*',
	servers: [{
		name: 'zend2',
		url: 'https://zend2.com/includes/process.php?action=update1',
		postData: {u: '__URL__', encodeURL: 'on', allowCookies: 'on', stripJS: 'on', stripObjects: 'on'}
	}]
}];
