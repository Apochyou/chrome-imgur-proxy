console.log('content massager invoked.');

chrome.runtime.onMessage.addListener(
	function(request, sender, sendResponse) {
		//console.log(request, sender);
		if (sender.id != chrome.runtime.id)
			return;
		// request = {url: xxx, proxyURL: xxx}
		var img = document.querySelectorAll('[src="' + request.url + '"]');
		if (img) {
			//console.debug(request.url + ' -> ' + request.proxyURL);
			for (var i = 0; i < img.length; i++)
				img[i].src = request.proxyURL;
		}
});