if (window.imgurTimeout)
	clearTimeout(window.imgurTimeout);

if (!window.scripts)
	window.scripts = Array();

window.scripts.push(document.currentScript);

window.imgurTimeout = setTimeout(function() {
	var script;
	while (script = window.scripts.pop()) {
		var imgBlock = script.parentElement.getElementsByClassName('imgur-embed-pub')[0];
		script.parentElement.removeChild(script);
		//console.log(imgBlock);
		if (!imgBlock) {
			console.log("NO imgur block???");
			continue;
		}
		var a = imgBlock.getElementsByTagName('a')[0];
		var imgurURL = a.protocol + '//i.imgur.com' + a.pathname + '.jpg';
		var img = document.createElement('img');
		img.src = imgurURL;
		img.style.width = '100%';
		img.style.height = '100%';
		img.style['max-width'] = '100%';
		img.style['max-height'] = '100%';
		imgBlock.parentElement.appendChild(img);
	}
}, 1500);

